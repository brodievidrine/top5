<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
	<sec:csrfMetaTags />
	<title>Ascend Top 5</title>
	<link rel="icon"
	      type="image/png"
	      href="resources/images/favicon.png">
	<link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/bigBoard.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/manageItems.css" />
	
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/manageUsers.js"></script>

</head>
<body>
	<div id="headerBar">
		<div class=floater>
			<div class="topLogo">Ascend Top 5</div>
				<sec:authorize access="hasAnyRole('ROLE_USER', 'ROLE_ADMIN')">
					<!-- For login user -->
					<c:url value="/logout" var="logoutUrl" />
					<form action="${logoutUrl}" method="post" id="logoutForm">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					</form>
					<script>
						function formSubmit() {
							document.getElementById("logoutForm").submit();
						}
					</script>
					
					<c:if test="${pageContext.request.userPrincipal.name != null}">
						<div class="topNav">
							<div>
							User: ${dName} 
							</div>
							<a href="home">Home</a> |
							<a href="addItems">Manage Items</a> |
							<a href="javascript:formSubmit()"> Logout</a>
						</div>
					</c:if>
				</sec:authorize>
				<sec:authorize access="isAnonymous()">
					<div class="topNav"> 
						<a id="logLink" href="login.html">Sign In</a>
					</div>
				</sec:authorize>
		</div><!-- End Nav Floater -->
	</div><!-- End Header Bar -->
	<div class="floater"><!-- Board Floater -->
	<div id="bigBoard">
		<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
		<div class="inner">
		<div class="panel" id="listBox">
			<h2>All Users</h2>
		   	<div id="userPropTableDiv" >
			   	<table id="userPropTable">
			        <thead>
			        	<tr>
			        	<th>Display Name</th><th>User</th><th>Pass</th><th>Role</th><th>Enable</th><th>Edit</th><th>Delete</th>
			        	</tr>
			        </thead>
			        <tbody>
			        <c:forEach var="user" items="${allUsers}">
			        	<tr id="${user.getUserName()}" >
			        		<td class="userCols dName">${user.getDisplayName()}</td>
			        		<td class="userCols">${user.getUserName()}</td>
			        		<td class="userCols pWord">${user.getPassword()}</td>
			        		<td class="userCols">
			        			<select onchange="authorizeUser(this)">
			        				<option value="${user.getRole()}">${user.getRoleDName()}</option>
			        				<option value="ROLE_ADMIN">Admin</option>
			        				<option value="ROLE_USER">User</option>
			        			</select>
			        		</td>
			        		<td class="userCols">
			        			<select onchange="enableUser(this)">
			        				<option value="${user.getEnabled()}">${user.getEnabled()}</option>
			        				<option value="${!user.getEnabled()}">${!user.getEnabled()}</option>
			        			</select>
			        		</td>
			        		<td class="userCols"><div class="listEdit" onclick="sendToEditor(this)" ><img src="resources/images/edit.png"/></div></td>
			        		<td  class="userCols"><div id="deleteCol" ><div class="listDelete" onclick="deleteUser(this)" >x</div></div></td>
			        		</tr>
			        </c:forEach>
			        </tbody>
				</table><!-- end userPropTable -->
			</div><!-- End userPropTableDiv -->
		</div><!-- ListBox -->
		<div class="panel" id="inputBox">
			<h2 id="formHeader">Create New User</h2>
			<div id="addNew" class ="formWrapper">
			<form:form method="POST" action="">
			   <table>
			   <tr>
			        <form:label path="displayName">Display Name</form:label>
			        <form:input id="dName" path="displayName" />
			    </tr>
			   <tr>
			        <form:label path="userName">User Name</form:label>
			        <form:input id="uName" path="userName" />
			    </tr>
			    <tr>
			        <form:label path="password">Password</form:label>
			        <form:input id="pWord" path="password" />
			    </tr>
			    <tr>
			    <tr>
			        <td colspan="2">
			            <input type="button" onclick="addUser()" value="Submit"/>
			        </td>
			    </tr>
			</table>  
			</form:form>
			</div><!-- End Form Wrapper-->
			<div id="editItem" class="formWrapper">
			<form:form method="POST" action="">
			   <table>
			   <tr>
			        <form:label path="displayName">Display Name</form:label>
			        <form:input id="editDName" path="displayName" />
			    </tr>
			    <tr>
			        <form:label path="password">Password</form:label>
			        <form:input id="editPWord" path="password" />
			    </tr>
			    <tr>
			    <tr>
			        <td colspan="2">
			        	<input id="uID" type="hidden" value=""/>
			        	<input type="button" onclick="hideEdit()" value="Cancel"/>
			            
			        </td>
			        <td><input type="button" onclick="updateUser()" value="Submit"/></td>
			    </tr>
			</table>  
			</form:form>
			</div><!-- End Form Wrapper-->
		</div>
		</div>
	</sec:authorize>
	</div><!-- end bigBoard -->
</div><!-- end floater -->
<div id="bbFooter" class="footer">Ascend Education, Copyright 2015. All rights reserved.</div>
</body>
</html>