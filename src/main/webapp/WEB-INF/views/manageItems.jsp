<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
	<sec:csrfMetaTags />
	<title>Ascend Top 5</title>
	<link rel="icon"
	      type="image/png"
	      href="resources/images/favicon.png">
	<link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/bigBoard.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/manageItems.css" />
	
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/manageItems.js"></script>

</head>
<body>
	<div id="headerBar">
		<div class=floater>
			<div class="topLogo">Ascend Top 5</div>
				<sec:authorize access="hasAnyRole('ROLE_USER', 'ROLE_ADMIN')">
					<!-- For login user -->
					<c:url value="/logout" var="logoutUrl" />
					<form action="${logoutUrl}" method="post" id="logoutForm">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					</form>
					<script>
						function formSubmit() {
							document.getElementById("logoutForm").submit();
						}
					</script>
					
					<c:if test="${pageContext.request.userPrincipal.name != null}">
						<div class="topNav">
							<div>
							User: ${dName} 
							</div>
							<a href="home">Home</a> |
							<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
								<a href="manageUsers">Manage Users</a> |
							</sec:authorize>
							<a href="javascript:formSubmit()"> Logout</a>
						</div>
					</c:if>
				</sec:authorize>
				<sec:authorize access="isAnonymous()">
					<div class="topNav"> 
						<a id="logLink" href="login.html">Sign In</a>
					</div>
				</sec:authorize>
		</div><!-- End Nav Floater -->
	</div><!-- End Header Bar -->
	<div class="floater"><!-- Board Floater -->
	<div id="bigBoard">
		<div class="inner">
		<div class="panel" id="listBox">
			<span id="aiLabel">All Items</span>
			<div id="updateConfirmDiv">
				<c:if test="${ needConfirmUpdate }">
					<div id="checkBoxDiv"><input type="checkbox" onchange="updateConfirm()"/> Confirm for <span>${ thisWeek }</span></div>
				</c:if>
				<c:if test="${ !needConfirmUpdate }">
					Confirmed for the week of <span>${ thisWeek }</span>.
				</c:if>
			</div>
		   	<ol id="allItemsList">
		   		
		        <c:forEach var="item" items="${allItems}">
		        <c:if test="${not empty item}">
		        <li id="${item.getItem_id() }" class="${item.getColor()}" >
		        	<div class="listBlockDisplay">
		        		<div class="colorChip" >${item.getColor()}</div>
			        	<div class="title" >${item.getTitle()}</div>
			        	<div class="message" > ${item.getMessage()}</div>
			        </div>
			        <div class="listBlockButton">    
				        <!-- Delete Button, Wrapped in a form -->
				        <div id="${item.getItem_id() }" class="listEdit" onclick="sendToEditor(this.id)" ><img src="resources/images/edit.png"/></div>
		        	</div>
				    <div class="listBlockButton">    
				        <!-- Delete Button, Wrapped in a form -->
				        <div id="${item.getItem_id() }" class="listDelete" onclick="sendToFurnace(this.id)" >x</div>
		        	</div>
		        </li>
		        </c:if>
		        </c:forEach>
			</ol>
			<div>(Rearrange item priorities by dragging them into place.)</div>
		</div>
		<div class="panel" id="inputBox">
			<h2 id="formHeader">Create New Item</h2>
			<div id ="addNew" class="formWrapper">
			<form:form method="POST" action="">
			        <form:label path="title">Title</form:label>
			        <form:input id="inputTitle" class="inputTitle" path="title" maxlength="32" />
			  
			        <form:label path="message">Description</form:label>
			        <form:textarea id="description" class="descrip" path="message" type="textarea" />
					<div class="dropdownCorale" >
				        <div class="ddcUnit">
					        <form:label path="priority">Priority</form:label>
					        <form:input id="priority" type="number" min="1" max="${nextNumber}" value="${nextNumber}" path="priority" />
						</div>
						<div class="ddcUnit">
							<form:label path="color">Color Code</form:label>
					        <form:select id="setColor" path="color" onchange="editChangesMade()">
					        	<form:option value="white"/>
					        	<form:option class="red" value="red"/>
					        	<form:option class="yellow" value="yellow"/>
					        	<form:option class="green" value="green"/>
					        </form:select>
				        </div>
					</div>
					<div id="submitDiv">
		            <input onclick="insertNew()" type="button" value="Submit"/>
		            </div>
			</form:form>
			</div><!-- End Form Wrapper-->
			<div id="editItem" class="formWrapper">
			<form:form  method="POST" action="">
			  
			        <form:label path="title">Title</form:label>
			        <form:input id="editTitle" class="inputTitle" path="title" maxlength="32" onchange="editChangesMade()"/>
			  
			        <form:label path="message">Description</form:label>
			        <form:textarea id="editDescription" class="descrip" path="message" type="textarea" onchange="editChangesMade()" />
			        
			        <form:label path="color">Color Code</form:label>
			        <form:select id="editColor" path="color" onchange="editChangesMade()">
			        	<form:option value="white"/>
			        	<form:option class="red" value="red"/>
			        	<form:option class="yellow" value="yellow"/>
			        	<form:option class="green" value="green"/>
			        </form:select>
					
					<div id="submitDiv">
		            	
		            	<div class="inputBtn"><input onclick="hideEdit()" type="button" value="Cancel"/></div>
		            	<div class="inputBtn"><input type="button" onClick="sendUpdate()" value="Save"/></div>
		            
		            </div>
		            <input type="hidden" id="editItem_id" path="item_id" value="blank" />
		            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</form:form>
			</div><!-- End Form Wrapper-->
			
		</div>
		</div>
	</div><!-- end bigBoard -->
</div><!-- end floater -->
<div id="bbFooter" class="footer">Ascend Education, Copyright 2015. All rights reserved.</div>
</body>
</html>