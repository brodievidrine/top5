<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Ascend Top 5</title>
<link rel="icon" 
      type="image/png" 
      href="resources/images/favicon.png">
<script src="resources/js/jquery.js"></script>
<script src="resources/js/bigBoard.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/bigBoard.css" />
</head>
<body>
	<div id="headerBar">
		<div class=floater>
			<div class="topLogo">${title}</div>
				<sec:authorize access="hasAnyRole('ROLE_USER', 'ROLE_ADMIN')">
					<!-- For login user -->
					<c:url value="/logout" var="logoutUrl" />
					<form action="${logoutUrl}" method="post" id="logoutForm">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					</form>
					<script>
						function formSubmit() {
							document.getElementById("logoutForm").submit();
						}
					</script>
					
					<c:if test="${pageContext.request.userPrincipal.name != null}">
						<div class="topNav">
							<div>
							User: ${dName} 
							</div>
							<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
								<a href="manageUsers">Manage Users</a> |
							</sec:authorize>
							<a href="addItems">Manage Items</a> |
							<a href="javascript:formSubmit()"> Logout</a>
						</div>
					</c:if>
				</sec:authorize>
				<sec:authorize access="isAnonymous()">
					<div class="topNav"> 
						<a id="logLink" href="login.html">Sign In</a>
					</div>
				</sec:authorize>
		</div><!-- End Nav Floater -->
	</div><!-- End Header Bar -->
	<div class="floater"><!-- Board Floater -->
	<div id="bigBoard">
<c:forEach var="user" items="${allUsers}" varStatus="loop">
	<c:if test="${user != null }">
	<div class="fpUserBox">
		<div class="fpUser" > <span class="fpHeader">${user.getDisplayname()} - ${user.getForWeek()}</span> </div>
			<div class="miniListDiv">
			<ol class="miniList" >
				<c:forEach var="itemTitle" items="${user.getItemTitles()}" varStatus="loop">
				<c:if test="${itemTitle != null }">
				<c:set var="color" value="${ user.getItemColors()[loop.index] }"/>
				<c:set var="descrip" value="${ user.getItemDescriptions()[loop.index] }"/>
				<li>
					<div class="liDivContainer"  >
						<span class="${color}" >
							<span class="titleMeasure" title="${descrip}" > ${itemTitle}</span>
							</span>
						</div>
				</li>
				</c:if>
				</c:forEach>
			</ol>
			</div>
	</div>
	</c:if>
	</c:forEach>
	</div><!-- end bigBoard -->
</div><!-- end floater -->
<div id="bbFooter" class="footer">Ascend Education, Copyright 2015. All rights reserved.</div>
</body>
</html>