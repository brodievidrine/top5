<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html >
<head>
    <title>Manage Items</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="resources/css/manageItems.css" />
</head>
<body>
<sec:authorize access="hasAnyRole('ROLE_USER', 'ROLE_ADMIN')">
	<div id="outer" class="floater">
	<div id="inner">
	<div>
		<a href="home">Home</a>
	</div>
	<h1>Manage Users</h1>
	<div class="panel" id="listBox">
		<h2>All Users</h2>
	   	<table id="userPropTable">
	        <thead>
	        	<tr>
	        	<th>Display Name</th><th>User</th><th>Pass</th><th>Enabled</th>
	        	</tr>
	        </thead>
	        <tbody>
	        <c:forEach var="user" items="${allUsers}">
	        	<tr>
	        		<td class="userCols">${user.getDisplayName()}</td>
	        		<td class="userCols">${user.getUserName()}</td>
	        		<td class="userCols">${user.getPassword()}</td>
	        		<td class="userCols">${user.getEnabled()}</td>
	        		</tr>
	        </c:forEach>
	        </tbody>
		</table>
	</div>
	<div class="panel" id="inputBox">
		<h2>Create New User</h2>
		<c:url var="addUrl" value="/manageUsers"/>
		<form:form id="formWrapper" method="POST" action="${addUrl}">
		   <table>
		   <tr>
		        <form:label path="displayName">Display Name</form:label>
		        <form:input path="displayName" />
		    </tr>
		   <tr>
		        <form:label path="userName">User Name</form:label>
		        <form:input path="userName" />
		    </tr>
		    <tr>
		        <form:label path="password">Password</form:label>
		        <form:input path="password" />
		    </tr>
		    <tr>
		    <tr>
		        <td colspan="2">
		            <input type="submit" value="Submit"/>
		            <input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
		        </td>
		    </tr>
		</table>  
		</form:form>
	</div>
</div><!-- #inner.floater -->
</div> <!-- #outer.floater -->
</sec:authorize> 
</body>
</html>