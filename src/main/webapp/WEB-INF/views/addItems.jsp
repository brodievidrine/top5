<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html >
<head>
    <title>Manage Items</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <sec:csrfMetaTags />
    <link rel="icon" type="image/png" href="resources/images/favicon.png">
    <link rel="stylesheet" type="text/css" href="resources/css/manageItems.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css" />
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/manageItems.js"></script>
</head>
<body>
<sec:authorize access="hasAnyRole('ROLE_USER', 'ROLE_ADMIN')">
	<div id="outer" class="floater">
	<div id="inner">
	<div>
		<a href="home">Home</a>
	</div>
	<h1>Manage Items</h1>
	<div class="panel" id="listBox">
		<h2>All Items</h2>
		<h3>Rearrange item priorities by dragging them into place.</h3>
	   	<ol id="allItemsList">
	   		
	        <c:forEach var="item" items="${allItems}">
	        <c:if test="${not empty item}">
	        <li id="${item.getItem_id() }">
	        	<div class="listBlockDisplay">
		        	<div class="title" >${item.getTitle()}</div>
		        	<div class="message" > ${item.getMessage()}</div>
		        </div>
			    <div class="listBlockButton">    
			        <!-- Delete Button, Wrapped in a form -->
			        <button id="${item.getItem_id() }" class="listDelete" onclick="sendToFurnace(this.id)" >Delete</button>
	        	</div>
	        </li>
	        </c:if>
	        </c:forEach>
		</ol>
		
	</div>
	<div class="panel" id="inputBox">
		<h2>Create New Item</h2>
		<c:url var="addUrl" value="/addItems"/>
		<form:form id="formWrapper" method="POST" action="${addUrl}">
		   <table>
		   <tr>
		        <form:label path="title">Title</form:label>
		        <form:input id="inputTitle" path="title" />
		    </tr>
		    <tr>
		        <form:label path="message">Description</form:label></td>
		        <form:textarea id="description" path="message" type="textarea" />
		    </tr>
		    <tr>
		        <form:label path="priority">Priority</form:label>
		        <form:input id="priority" type="number" min="1" max="${nextNumber}" value="${nextNumber}" path="priority" />
		    </tr>
		    <tr>
		    <tr>
		        <td colspan="2">
		            <input type="submit" value="Submit"/>
		            <input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
		        </td>
		    </tr>
		</table>  
		</form:form>
	</div>
</div><!-- #inner.floater -->
</div> <!-- #outer.floater -->
</sec:authorize> 
</body>
</html>