$( window ).load(function() {
	
	sizeBoard();
	resizeFPHeaders();
	resizeItems();
	
	$( window ).resize( function(){
		sizeBoard();
	} );

});
/*
$( ".titleMeasure" ).ready(function() {
	console.log("firing resizing torpedos!");
	while( $(this).width() > $(this).parent().parent().width() - 6 ){
		var fontSize= parseInt( $(this).css("fontSize"), 10);
		fontSize--;
		$(this).css("fontSize", fontSize);
	}
});

$( ".fpHeader" ).ready(function() {
	
	while( $(this).width() > $(this).parent().parent().width() - 6 ){
		var fontSize= parseInt( $(this).css("fontSize"), 10);
		fontSize--;
		$(this).css("fontSize", fontSize);
	
	}
});
*/
function sizeBoard( ){
	var minBoardHeight = 500;
	var footerYmin=667;
	var nonBoard = (200 );//<-- Extremely scientific
	var winH = window.innerHeight;
	var boardHeight = winH - nonBoard;
	if(boardHeight > 500)
		$("#bigBoard").css("height", String( boardHeight )+"px");
	
	var footerTop = winH - 40;
	if(footerTop < 667)
		footerTop = 667;
	$("#bbFooter").css({
		"top":String( footerTop ) + "px"
	});
	
}

function resizeItems(){
	
	$( ".titleMeasure" ).each(function(){
		
		while( $(this).width() > $(this).parent().parent().width() - 6 ){
			var fontSize= parseInt( $(this).css("fontSize"), 10);
			fontSize--;
			$(this).css("fontSize", fontSize);
		}
	});
}


function resizeFPHeaders(){
	
	$( ".fpHeader" ).each(function(){
		
		while( $(this).width() > $(this).parent().parent().width() - 6 ){
			var fontSize= parseInt( $(this).css("fontSize"), 10);
			fontSize--;
			$(this).css("fontSize", fontSize);
		
		}
	});
}