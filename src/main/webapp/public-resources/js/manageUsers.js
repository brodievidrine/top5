var deleteEnabled = true;
var editing = false;



$( document ).ready(function() {
	
	sizeBoard();
	
	$( window ).resize( function(){
		sizeBoard();
	} );
});

function addUser(){
	
	var json = {
		displayName: $("#dName").val(),
		userName: $("#uName").val(),
		password: $("#pWord").val()
	}
	
	genericAJAX( "manageUsers", json );

}

function updateUser(){
	
	var json = {
		userName:$("#uID").val(),
		displayName: $("#editDName").val(),
		password: $("#editPWord").val()
	}
	
	genericAJAX( "editUser", json );

}

function enableUser(select){
	
	var val = $(select).val();
	var id = ( $(select).parent().parent().attr("id") ); 
	var json = {
		
		userName: id,
		enabled: val
	}
	
	genericAJAX( "enableUser", json );

}

function authorizeUser(select){
	
	
	var val = $(select).val();
	var id = ( $(select).parent().parent().attr("id") ); 
	var json = {
		
		userName: id,
		role: val
	}
	
	genericAJAX( "authorizeUser", json );

}

function genericAJAX( action, json ){
	
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	
	$.ajax({
        url: action,
        type: 'POST',
        data: JSON.stringify(json),
        cache:false,
        beforeSend: function(xhr) {  
            xhr.setRequestHeader("Accept", "application/json");  
            xhr.setRequestHeader("Content-Type", "application/json"); 
            xhr.setRequestHeader(header, token);
        },
        success:function(response){
        	location.reload(true);
        },
        error:function(jqXhr, textStatus, errorThrown){
            console.log("failed ajax call");
        }
    });
	
}

function showEdit(){
	
	if ($('#addNew:visible')){
		$("#formHeader").html("Update User");
		$("#addNew").hide();
		$("#editItem").show();
	
	}	
}

function hideEdit(){
	
	$("#formHeader").html("Create New User");
	$("#addNew").show();
	$("#editItem").hide();
}

function sendToEditor( btn ){
	
	var id = ( $(btn).parent().parent().attr("id") );
	
	showEdit();
	var dName = $("#" + id + " .dName").html();
	var pWord = $("#" + id + " .pWord").html();
	
	$("#editDName").val(dName);
	$("#editPWord").val(pWord);
	$("#uID").val(id);
}

function deleteUser( btn ){
	
	var sure = confirm("Are you sure you want to delete that?");
	
	if(!sure) return;
	
	var id = ( $(btn).parent().parent().parent().attr("id") );
	var json = {
		userName:id
	}
	
	genericAJAX("deleteUser", json);
}

function passItemId(id){
	
	console.log( "ID =", id );
}

function sizeBoard( ){
	var minBoardHeight = 500;
	var footerYmin=667;
	var nonBoard = (200 );//<-- Extremely scientific 
	var winH = window.innerHeight;
	var boardHeight = winH - nonBoard;
	if(boardHeight > 500)
		$("#bigBoard").css("height", String( boardHeight )+"px");
	
	var footerTop = winH - 40;
	if(footerTop < 667)
		footerTop = 667;
	$("#bbFooter").css({
		"top":String( footerTop ) + "px"
	});
	
}