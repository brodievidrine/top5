var deleteEnabled = true;
var editing = false;
var changes2abandon = false;



$( document ).ready(function() {
	
	sizeBoard();
	
	$( window ).resize( function(){
		sizeBoard();
	} );
	
	$("#allItemsList").sortable({
		
		update: function(event, ui){
			$("#allItemsList").sortable("disable");
			var sortedIDs = $( "#allItemsList" ).sortable( "toArray" );
			sendPriorityList({
				prioritizedList: sortedIDs
			});
		}
	});
	
	$("#allItemsList").disableSelection();
});

function editChangesMade(){
	changes2abandon = true;
}

function sendPriorityList( json ){
	
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	
	$.ajax({
        url: "reOrderItems",
        type: 'POST',
        data: JSON.stringify(json),
        cache:false,
        beforeSend: function(xhr) {  
            xhr.setRequestHeader("Accept", "application/json");  
            xhr.setRequestHeader("Content-Type", "application/json"); 
            xhr.setRequestHeader(header, token);
        },
        success:function(response){
        	
        	$("#allItemsList").sortable("enable");
        
        },
        error:function(jqXhr, textStatus, errorThrown){
            console.log("failed ajax call");
        	//alert(textStatus);
        }
    });
	
}

function sendToFurnace( id ){
	
	var sure = confirm("Are you sure you want to delete that?");
	
	if(!deleteEnabled || !sure) return;
	deleteEnabled = false;
	
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	
	$.ajax({
        url: "deleteItem",
        type: 'POST',
        data: id,
        cache:false,
        beforeSend: function(xhr) {  
            xhr.setRequestHeader("Accept", "application/json");  
            xhr.setRequestHeader("Content-Type", "application/json"); 
            xhr.setRequestHeader(header, token);
        },
        success:function(response){
        	
        	//$("#allItemsList").sortable("enable");
        	location.reload(true);
        
        },
        error:function(jqXhr, textStatus, errorThrown){
            console.log("failed ajax call");
        	//alert(textStatus);
        }
    });
	
}

function showEdit(){
	
	if ($('#addNew:visible')){
		$("#formHeader").html("Update Item");
		$("#addNew").hide();
		$("#editItem").show();
	
	}	
}

function hideEdit(){
	
	if(changes2abandon){
		
		var sure = confirm("Abandon Changes?");
		if(!sure) return;
	}
	
	$("#formHeader").html("Create New Item");
	$("#addNew").show();
	$("#editItem").hide();
	changes2abandon = false;
}

function sendToEditor(id){
	
	if(changes2abandon){
		var sure = confirm("Abandon changes?");
		if(!sure) return;
	}
	
	showEdit();
	var title = $("#" + id + " .title").html();
	var message = $.trim( $("#" + id + " .message").html() );
	var colorCode = $("#" + id + " .colorChip").html();
	
	$("#editTitle").val(title);
	$("#editDescription").val(message);
	$("#editItem_id").val(id);
	$("#editColor").val(colorCode);
	changes2abandon = false;
	
}

function insertNew( ){
	
	
	var json={
		
		title : $("#inputTitle").val(),
		message:$("#description").val(),
		color:$("#setColor").val(),
		priority:$("#priority").val()
	}
	
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	
	$.ajax({
        url: "addItems",
        type: 'POST',
        data: JSON.stringify(json),
        cache:false,
        beforeSend: function(xhr) {  
            xhr.setRequestHeader("Accept", "application/json");  
            xhr.setRequestHeader("Content-Type", "application/json"); 
            xhr.setRequestHeader(header, token);
        },
        success:function(response){
        	
        	//$("#allItemsList").sortable("enable");
        	location.reload(true);
        
        },
        error:function(jqXhr, textStatus, errorThrown){
            console.log("failed ajax call");
        	//alert(textStatus);
        }
    });
	
}


function sendUpdate( ){
	
	var json={
		
		title : $("#editTitle").val(),
		message:$("#editDescription").val(),
		item_id:$("#editItem_id").val(),
		color:$("#editColor").val()
	}
	
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	
	$.ajax({
        url: "updateItem",
        type: 'POST',
        data: JSON.stringify(json),
        cache:false,
        beforeSend: function(xhr) {  
            xhr.setRequestHeader("Accept", "application/json");  
            xhr.setRequestHeader("Content-Type", "application/json"); 
            xhr.setRequestHeader(header, token);
        },
        success:function(response){
        	
        	//$("#allItemsList").sortable("enable");
        	location.reload(true);
        
        },
        error:function(jqXhr, textStatus, errorThrown){
            console.log("failed ajax call");
        	//alert(textStatus);
        }
    });
	
}

function passItemId(id){
	
	console.log( "ID =", id );
}

function updateConfirm(){
	
	var empty={};
	
	genericAJAX( "updateConfirm", empty);
}

function genericAJAX( action, json ){
	
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	
	$.ajax({
        url: action,
        type: 'POST',
        data: JSON.stringify(json),
        cache:false,
        beforeSend: function(xhr) {  
            xhr.setRequestHeader("Accept", "application/json");  
            xhr.setRequestHeader("Content-Type", "application/json"); 
            xhr.setRequestHeader(header, token);
        },
        success:function(response){
        	location.reload(true);
        },
        error:function(jqXhr, textStatus, errorThrown){
            console.log("failed ajax call");
        }
    });
	
}

function sizeBoard( ){
	var minBoardHeight = 500;
	var footerYmin=667;
	var nonBoard = (200 );//<-- Extremely scientific 
	var winH = window.innerHeight;
	var boardHeight = winH - nonBoard;
	if(boardHeight > 500)
		$("#bigBoard").css("height", String( boardHeight )+"px");
	
	var footerTop = winH - 40;
	if(footerTop < 667)
		footerTop = 667;
	$("#bbFooter").css({
		"top":String( footerTop ) + "px"
	});
	
}