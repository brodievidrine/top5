package com.top5.dao;

import com.top5.beans.Item;

public interface ItemDAO {
	public void delete(Item item);
	public void insert(Item item);
	public void deleteAllUserItems(String user);
	public void updateItem(Item item);
	public Item[] getItems(String user, int limitTo);
	public void updatePriorities(Item[] items);
}
