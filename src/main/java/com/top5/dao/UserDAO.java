package com.top5.dao;

import com.top5.beans.User;

public interface UserDAO {
	
	public void insert(User user);
	public void update(User user);
	public void updateConfirm(String username);
	public void enable(User user);
	public void authorize(User user);
	public void delete(User user);
	public User findByUserId(String user);
	public User[] getUserList(Boolean needRoles);
}
