package com.top5.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.top5.dao.ItemDAO;
import com.top5.beans.Item;

public class JdbcItemDAO implements ItemDAO {
	private DataSource dataSource;
		
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public void insert(Item item){
		
		String sql = "INSERT INTO items " +
				"(USERNAME, TITLE, MESSAGE, PRIORITY, COLOR) VALUES (?, ?, ?, ?, ?)";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, item.getUsername());
			ps.setString(2, item.getTitle());
			ps.setString(3, item.getMessage());
			ps.setInt(4, item.getPriority());
			ps.setString(5, item.getColor());
			
			ps.executeUpdate();
			ps.close();
 
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public void delete(Item item){
			
		String sql = "DELETE FROM items " +
                   "WHERE item_id = ?";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, item.getItem_id() );
			
			ps.executeUpdate();
			ps.close();
 
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public void updateItem(Item item){
		
		String sql = 
		"UPDATE items"+
		" SET TITLE=?, MESSAGE=?, COLOR=?"+
		" WHERE ITEM_ID=?";
		
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, item.getTitle() );
			ps.setString(2, item.getMessage() );
			ps.setString(3, item.getColor() );
			ps.setInt(4, item.getItem_id() );
			
			ps.executeUpdate();
			ps.close();
 
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public Item[] getItems(String user, int limitTo){
		
		String sql = "SELECT * FROM items WHERE username = ? ORDER BY PRIORITY";
		 
		Connection conn = null;
		
		try {
			
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user);
			//ps.setInt(1, custId);
			Item[] itemList = new Item[1];
			ResultSet rs = ps.executeQuery();
			int i = 0;
			while (rs.next()) {
				Item[] tempItemList = new Item[i+1];
				System.arraycopy(itemList, 0, tempItemList, 0, itemList.length);
				
				Item item = new Item();
				item.setItem_id( rs.getInt("ITEM_ID") );
				item.setUsername( rs.getString("USERNAME") );
				item.setTitle( rs.getString("TITLE") );
				item.setMessage( rs.getString("MESSAGE") );
				item.setColor( rs.getString("COLOR") );
				item.setPriority( rs.getInt("PRIORITY") );
				
				tempItemList[i] = item;
				itemList = new Item[tempItemList.length];
				System.arraycopy(tempItemList, 0, itemList, 0, tempItemList.length);
				i++;
				if(limitTo != -1 && i >= limitTo) break;//-1 means return all values, otherwise restrict to limit
			
			}
			rs.close();
			ps.close();
			return itemList;
		
		} catch (SQLException e) {
			throw new RuntimeException(e);
		
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					//Exception?
				}
			}
		}
		
	}
	
public void deleteAllUserItems(String user){
		System.out.println("running deleteAllUserItems");
		String sql = "DELETE FROM items WHERE username=?";
		//""UPDATE DEPT SET MGRNO=? WHERE DEPTNO=?"
		
		Connection conn = null;
		
		try {
			  conn = dataSource.getConnection();
			  conn.setAutoCommit(false);                   
			  PreparedStatement ps = conn.prepareStatement( sql );           
			                           
			  ps.setString(1, user );
			  ps.executeUpdate();
			  ps.close();
			  conn.commit();                                          
			} catch(SQLException e ) {
				System.out.println("dying here at 177");
			} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					//Exception?
				}
			}
		}
		
	}
	
	public void updatePriorities(Item[] items){
		
		String sql = "UPDATE items SET PRIORITY=? WHERE ITEM_ID=?";
		//""UPDATE DEPT SET MGRNO=? WHERE DEPTNO=?"
		
		Connection conn = null;
		
		try {
			  conn = dataSource.getConnection();
			  conn.setAutoCommit(false);                   
			  PreparedStatement prepStmt = conn.prepareStatement( sql );           
			  
			  for(int i=0;i<items.length;i++){
				  prepStmt.setInt(1, items[i].getPriority() );                         
				  prepStmt.setInt(2, items[i].getItem_id() );
				  prepStmt.addBatch();                                   
			  }
			  
			  //FYI.returns an array denoting how many rows were altered for each execution
			  //Useful for debugging
			  prepStmt.executeBatch();
			  conn.commit();                                          
			} catch(SQLException e ) {
			  // process BatchUpdateException
			} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					//Exception?
				}
			}
		}
		
	}
	
}