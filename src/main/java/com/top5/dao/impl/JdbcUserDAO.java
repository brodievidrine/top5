package com.top5.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.top5.beans.User;
import com.top5.dao.UserDAO;

public class JdbcUserDAO implements UserDAO {
	
private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
 
	public void insert(User user){
 
		String sql = "INSERT INTO users " +
				"(DISPLAYNAME, USERNAME, PASSWORD) VALUES (?, ?, ?)";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getDisplayName());
			ps.setString(2, user.getUserName());
			ps.setString(3, user.getPassword());
			ps.executeUpdate();
			ps.close();
			
			addRole(user);
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public void update(User user){
		 
		String sql = "UPDATE users" + " SET DISPLAYNAME=?, PASSWORD=?" + " WHERE USERNAME=?";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getDisplayName());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getUserName());
			ps.executeUpdate();
			ps.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public void enable(User user){
		 
		String sql = "UPDATE users" + " SET ENABLED=?" + " WHERE USERNAME=?";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setBoolean(1, user.getEnabled());
			ps.setString(2, user.getUserName());
			ps.executeUpdate();
			ps.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public void authorize(User user){
		 
		String sql = "UPDATE user_roles" + " SET ROLE=?" + " WHERE USERNAME=?";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getRole());
			ps.setString(2, user.getUserName());
			ps.executeUpdate();
			ps.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public void updateConfirm(String username){
		 
		String sql = "UPDATE users" + " SET updateConfirm=CURRENT_TIMESTAMP" + " WHERE USERNAME=?";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.executeUpdate();
			ps.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public void delete(User user){
		
		deleteUserRole(user);
		
		String sql = "DELETE FROM users WHERE USERNAME=?";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getUserName());
			ps.executeUpdate();
			ps.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public void deleteUserRole(User user){
		 
		String sql = "DELETE FROM user_roles WHERE USERNAME=?";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getUserName());
			ps.executeUpdate();
			ps.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public void addRole(User user){
		 
		String sql = "INSERT INTO user_roles " +
				"(USERNAME, ROLE) VALUES (?, 'ROLE_USER')";
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getUserName());
			ps.executeUpdate();
			ps.close();
 
		} catch (SQLException e) {
			throw new RuntimeException(e);
 
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
 
	public User findByUserId(String uName){
 
		String sql = "SELECT * FROM users WHERE USERNAME = ?";
 
		Connection conn = null;
 
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, uName);
			User user = null;
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setUserName( rs.getString("USERNAME") );
				user.setDisplayName( rs.getString("DISPLAYNAME") );
				user.setEnabled( rs.getBoolean("ENABLED") );
				user.setPassword( rs.getString("PASSWORD") );
				user.setUpdateConfirm( rs.getTimestamp("updateConfirm") );
			}
			rs.close();
			ps.close();
			return user;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	/*
	 getUserList() returns the complete list of users 
	 * */
	
	public User[] getUserList(Boolean needRoles){
		
		if(needRoles==null){
			needRoles=false;
		}
		
		String sql = "SELECT * FROM users";
 
		Connection conn = null;
 
		try {
			
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			User[] userList = new User[1];
			ResultSet rs = ps.executeQuery();
			int i = 0;
			while (rs.next()) {
				
				User[] tempCSList = new User[i+1];
				System.arraycopy(userList, 0, tempCSList, 0, userList.length);
				
				User user = new User();
				user.setUserName( rs.getString("USERNAME") );
				user.setDisplayName( rs.getString("DISPLAYNAME") );
				user.setEnabled( rs.getBoolean("ENABLED") );
				user.setPassword( rs.getString("PASSWORD") );
				user.setUpdateConfirm( rs.getTimestamp("updateConfirm") );
				tempCSList[i] = user;
				userList = new User[tempCSList.length];
				System.arraycopy(tempCSList, 0, userList, 0, tempCSList.length);
				i++;
			
			}
			if(needRoles){
				User[] roles = getUserRoles();
				
				for( User role: roles){
					
					for(User user:userList){
						if( user.getUserName().equals(role.getUserName() ) ){
							user.setRole( role.getRole() );
							break;
						}
					}
					
				}
			
			}
			
			rs.close();
			ps.close();
			return userList;
		
		} catch (SQLException e) {
			throw new RuntimeException(e);
		
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					//Exception?
				}
			}
		}
	}

	public User[] getUserRoles(){
			
		String sql = "SELECT * FROM user_roles";
 
		Connection conn = null;
 
		try {
			
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			User[] userList = new User[1];
			ResultSet rs = ps.executeQuery();
			int i = 0;
			while (rs.next()) {
				
				User[] tempCSList = new User[i+1];
				System.arraycopy(userList, 0, tempCSList, 0, userList.length);
				
				User user = new User();
				user.setUserName( rs.getString("USERNAME") );
				user.setRole( rs.getString("ROLE") );
				tempCSList[i] = user;
				userList = new User[tempCSList.length];
				System.arraycopy(tempCSList, 0, userList, 0, tempCSList.length);
				i++;
			
			}
			
			rs.close();
			ps.close();
			return userList;
		
		} catch (SQLException e) {
			throw new RuntimeException(e);
		
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					//Exception?
				}
			}
		}
	}
}
