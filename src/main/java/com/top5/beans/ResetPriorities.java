package com.top5.beans;

public class ResetPriorities {
	private int[] prioritizedList;
	
	public int[] getPrioritizedList(){
		return prioritizedList;
	}
	
	public void setPrioritizedList(int[] pList){
		this.prioritizedList = pList;
	}
}
