package com.top5.beans;

public class BigBoardUser {
	private String displayname = "Default";
	private String forWeek = "2/29";
	private String[] itemTitles = new String[5];
	private String[] itemDescriptions = new String[5];
	private String[] itemColors = new String[5];
	
	public String getDisplayname(){
		return displayname;
	}
	
	public void setDisplayname(String dName){
		this.displayname = dName;
	}
	
	public String[] getItemTitles(){
		return itemTitles;
	}
	
	public void setItemTitles(String[] titles){
		for(int i=0;i<titles.length;i++){
			this.itemTitles[i] = titles[i];
		}
	}
	
	public String[] getItemDescriptions(){
		return itemDescriptions;
	}
	
	public void setItemDescriptions(String[] descrip){
		for(int i=0;i<descrip.length;i++){
			this.itemDescriptions[i] = descrip[i];
		}
	}
	
	public String[] getItemColors(){
		return itemColors;
	}
	
	public void setItemColors(String[] descrip){
		for(int i=0;i<descrip.length;i++){
			
			this.itemColors[i] = descrip[i];
		}
	}
	
	public String getForWeek(){
		return forWeek;
	}
	
	public void setForWeek(String wk){
		this.forWeek = wk;
	}
}
