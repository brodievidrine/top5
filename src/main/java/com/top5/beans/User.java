package com.top5.beans;

import java.sql.Timestamp;
import com.top5.util.NearestMonday;

public class User {

	String userName;
	String displayName;
	String password;
	String role;
	Boolean enabled;
	Timestamp updateConfirm;
	
	public String getUserName(){
		return userName;
	}
	
	public void setUserName(String uID){
		this.userName = uID;
	}
	
	public String getDisplayName(){
		return displayName;
	}
	
	public void setDisplayName(String dID){
		this.displayName = dID;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String dID){
		this.password = dID;
	}
	
	public void setEnabled(Boolean bool){
		this.enabled = bool;
	}
	
	public Boolean getEnabled(){
		return enabled;
	}
	
	public String getRole(){
		return role;
	}
	
	public void setRole(String r){
		this.role = r;
	}
	
	public Timestamp getUpdateConfirm(){
		return updateConfirm;
	}
	
	public void setUpdateConfirm(Timestamp uC){
		this.updateConfirm = uC;
	}
	
	public String getRoleDName(){
		String roleDName="User";
		if( this.role.equals( "ROLE_ADMIN" ) )
			roleDName="Admin";
		
		return roleDName;
	}
	
	public String getForWeek(){
		
		NearestMonday nm = new NearestMonday();
		return nm.closestToTimestamp( updateConfirm );
	}
}