package com.top5.beans;

import java.sql.Timestamp;

public class Item {
	private int item_id;
	private int priority;
	private String username;
	private String title;
	private String message;
	private String color;
	private Timestamp posted_on;
	
	//getters done
	public int getItem_id(){
		return item_id;
	}
	
	public void setItem_id(int id){
		this.item_id = id;
	}
	
	public int getPriority(){
		return priority;
	}
	
	public void setPriority(int p){
		priority = p;
	}
	
	public String getUsername(){
		return username;
	}
	
	public void setUsername(String u){
		username = u;
	}
	
	public String getTitle(){
		return title;
	}
	
	public void setTitle(String t){
		title = t;
	}
	
	public String getMessage(){
		return message;
	}
	
	public void setMessage(String m){
		message = m;
	}
	
	public String getColor(){
		return color;
	}
	
	public void setColor(String c){
		this.color = c;
		
	}
	
	public Timestamp getPostedOn(){
		return posted_on;
	}
	
	public void setPostedOn(Timestamp t){
		posted_on = t;
	}
}