package com.top5.util;

import java.util.Map;



public class EnvVars {
	
	public String getEVarByName(String attr) {
        Map<String, String> env = System.getenv();
        String val = env.get(attr);
        
        return val;
    }
	
	public String getDbUrl(String dbHost, String dbPort, String dbName){
		
		String host = this.getEVarByName(dbHost);
		String port = this.getEVarByName(dbPort);
		String name = this.getEVarByName(dbName);
		//"jdbc:mysql://localhost:3306/top5proto"
		String url = "jdbc:mysql://" + host + ":" + port + "/" + name;
		
		return url;
	}
}
