package com.top5.util;

import java.sql.Timestamp;
import java.util.Calendar;

public class NearestMonday {
	
	public String closestToTimestamp(Timestamp timestamp){
		
		String weekFor = "1/1";
		
		Calendar nearestMonday = Calendar.getInstance();
		nearestMonday.setTime( timestamp );
		
		//Determine which Monday we are closest to
		//Sun=1, Mon=2, Tues=3, Wed=4, Thurs=5, Fri=6, Sat=7
		int doW = nearestMonday.get(Calendar.DAY_OF_WEEK);//doW <-- Day of Week
		//If it is Friday, Saturday, or Sunday: ADVANCE
		if( doW >= 6 || doW < 2 ){
			int newDay=doW;
			while(newDay != 2 ){
				nearestMonday.add(Calendar.DATE, 1);
				newDay = nearestMonday.get(Calendar.DAY_OF_WEEK);
			}
			
		}//If it is Tues, Wednesday, or Thursday: DECREASE 
		else if(doW >=3 && doW < 6){
			
			int newDay=doW;
			while(newDay != 2 ){
				nearestMonday.add(Calendar.DATE, -1);
				newDay = nearestMonday.get(Calendar.DAY_OF_WEEK);
			}
		
		}//No else needed. If it was filled out on a Monday, it speaks for itself
		
		//Get Month
		Integer month = nearestMonday.get( Calendar.MONTH  ) + 1;
		Integer day = nearestMonday.get( Calendar.DATE);
		
		weekFor = Integer.toString( month ) +"/" + Integer.toString(day);
		
		
		return weekFor;
	}
	
	public String closestToNow(){
			
			String weekFor = "1/1";
			Calendar nearestMonday = Calendar.getInstance();
			
			//Determine which Monday we are closest to
			//Sun=1, Mon=2, Tues=3, Wed=4, Thurs=5, Fri=6, Sat=7
			int doW = nearestMonday.get(Calendar.DAY_OF_WEEK);//doW <-- Day of Week
			//If it is Friday, Saturday, or Sunday: ADVANCE
			if( doW >= 6 || doW < 2 ){
				int newDay=doW;
				while(newDay != 2 ){
					nearestMonday.add(Calendar.DATE, 1);
					newDay = nearestMonday.get(Calendar.DAY_OF_WEEK);
				}
				
			}//If it is Tues, Wednesday, or Thursday: DECREASE 
			else if(doW >=3 && doW < 6){
				
				int newDay=doW;
				while(newDay != 2 ){
					nearestMonday.add(Calendar.DATE, -1);
					newDay = nearestMonday.get(Calendar.DAY_OF_WEEK);
				}
			
			}//No else needed. If it was filled out on a Monday, it speaks for itself
			
			//Get Month
			Integer month = nearestMonday.get( Calendar.MONTH  ) + 1;
			Integer day = nearestMonday.get( Calendar.DATE);
			
			weekFor = Integer.toString( month ) +"/" + Integer.toString(day);
			
			
			return weekFor;
		}
}
