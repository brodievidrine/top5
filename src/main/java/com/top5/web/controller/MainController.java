package com.top5.web.controller;


import java.io.IOException;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;

import com.top5.beans.Item;
import com.top5.beans.User;
import com.top5.model.BigBoard;
import com.top5.model.ManageItems;
import com.top5.model.ManageUsers;

@Controller
public class MainController {
	
	@RequestMapping(value = {"*", "/home"}, method = RequestMethod.GET)
	public ModelAndView defaultPage(WebRequest webRequest) {
		
		BigBoard bigBoard = new BigBoard();
		ModelAndView model = bigBoard.getModel();
		
		return model;
	
	}
	
	
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}
	
	@RequestMapping(value = "/addItems", method = RequestMethod.GET)
	public ModelAndView manageItemsPage() {
      	
		//Get all of the users items
		String username="";
	  	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  	if (!(auth instanceof AnonymousAuthenticationToken)) {
	  		UserDetails userDetail = (UserDetails) auth.getPrincipal();
	  		username = userDetail.getUsername();
	  	}
	  	
	  	ManageItems manageItems = new ManageItems();
	  	ModelAndView model = manageItems.createManageItemsPage(username);
		
		return model;
   }
	
	@RequestMapping( value="/addItems", method = RequestMethod.POST)
	@ResponseBody   
	public void addItems(@RequestBody String json) throws IOException{
		
			//Prepare to send the new item to the db
		  	String username = "alanSmithee";
		  	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		  	if (!(auth instanceof AnonymousAuthenticationToken)) {
		  		UserDetails userDetail = (UserDetails) auth.getPrincipal();
		  		username = userDetail.getUsername();
		  	} 
		  	
		  	ManageItems manageItems = new ManageItems();
		  	manageItems.postItem(json, username);
		  	
	   }
	
	@RequestMapping( value="/reOrderItems", method = RequestMethod.POST)
	@ResponseBody
	public void reOrderItems(@RequestBody String json) throws IOException{
		
		ManageItems manageItems = new ManageItems();
		manageItems.reOrderItems( json );
	}
	
	@RequestMapping( value="/deleteItem", method = RequestMethod.POST)
	@ResponseBody
	public void deleteItem(@RequestBody int id/*, ModelMap model*/) throws IOException{
		
		//UNIT TEST: See if we're getting those parameters across
		System.out.println(id);
		
		ManageItems manageItems = new ManageItems();
		manageItems.deleteItem( id );
	}
	
	
	
	@RequestMapping(value = "/manageUsers", method = RequestMethod.GET)
	public ModelAndView manageUsersPage() {
	  	
	  	ManageUsers manageUsers = new ManageUsers();
	  	ModelAndView model = manageUsers.getManageUsersPage();
		
		return model;
   }
	
	@RequestMapping( value="/manageUsers", method = RequestMethod.POST)
	@ResponseBody
	public void addUser(@RequestBody String json) throws IOException{
		
		//Prepare to send the new item to the db 
	  	ManageUsers manageUsers = new ManageUsers();
	  	manageUsers.postUser( json );
	  	
	   }
	
	@RequestMapping( value="/editUser", method = RequestMethod.POST)
	@ResponseBody
	public void updateUser(@RequestBody String json) throws IOException{
		
		//Prepare to send the new item to the db 
	  	ManageUsers manageUsers = new ManageUsers();
	  	manageUsers.updateUser( json );
	  	
	   }
	
	@RequestMapping( value="/updateConfirm", method = RequestMethod.POST)
	@ResponseBody
	public void updateConfirm(){
		
		String username="";
	  	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  	if (!(auth instanceof AnonymousAuthenticationToken)) {
	  		UserDetails userDetail = (UserDetails) auth.getPrincipal();
	  		username = userDetail.getUsername();
	  	}
		
	  	//Prepare to send the new item to the db 
	  	ManageUsers manageUsers = new ManageUsers();
	  	manageUsers.updateConfirm( username );
	  
	   }
	
	@RequestMapping( value="/deleteUser", method = RequestMethod.POST)
	@ResponseBody
	public void deleteUser(@RequestBody String json) throws IOException{
		
		//Prepare to send the new item to the db 
	  	ManageUsers manageUsers = new ManageUsers();
	  	manageUsers.deleteUser( json );
	  	
	   }
	
	@RequestMapping( value="/enableUser", method = RequestMethod.POST)
	@ResponseBody
	public void enableUser(@RequestBody String json) throws IOException{
		
		//Prepare to send the new item to the db 
	  	ManageUsers manageUsers = new ManageUsers();
	  	manageUsers.enableUser( json );
	  	
	   }
	
	@RequestMapping( value="/authorizeUser", method = RequestMethod.POST)
	@ResponseBody
	public void authorizeUser(@RequestBody String json) throws IOException{
		
		//Prepare to send the new item to the db 
	  	ManageUsers manageUsers = new ManageUsers();
	  	manageUsers.authorizeUser( json );
	  	
	   }
	
	//for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();
		
		//check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			System.out.println(userDetail);
		
			model.addObject("username", userDetail.getUsername());
			
		}
		
		model.setViewName("403");
		return model;

	}
	
	@RequestMapping( value="/updateItem", method = RequestMethod.POST)
	@ResponseBody
	public void updateItem(@RequestBody String json) throws IOException{
		ManageItems manageItems = new ManageItems();	
	    manageItems.updateItem(json);
	}
	
	/*
	@RequestMapping(method = RequestMethod.POST)
	public void getAll(WebRequest webRequest){
	    
	    //...
	    System.out.println( "detecting illegal post request" + webRequest.getDescription(false) );
	}
	*/
	/*
	 * 
	 *FOR DEBUGGING REQUESTS WITH WEIRD VALUES 
	 */ 
	
	@RequestMapping(value="/405", method = RequestMethod.POST)
	public ModelAndView catchAllGETS(WebRequest webRequest) {
		
		System.out.println("This is the all purpose GET.");
		System.out.println( "caught this here GET a tryin to come through " + webRequest.getDescription(false) );
		ModelAndView model = new ModelAndView();
		//model.addObject("allUsers", fpUsers);
		model.setViewName("404");
		return model;
	}
	

}