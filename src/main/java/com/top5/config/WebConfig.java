package com.top5.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration 
public class WebConfig extends WebMvcConfigurerAdapter {
      
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/resources/**").addResourceLocations("/public-resources/");
  }
  
  @Controller  
  static class FaviconController {
	  
	  @RequestMapping("favicon.ico")
	  String favicon() {
		  return "forward:/resources/images/favicon.ico";
	  }
	 
  }

  
}