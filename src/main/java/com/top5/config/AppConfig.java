package com.top5.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;


//import com.top5.util.EnvVars;


@EnableWebMvc
@Configuration
@ComponentScan({ "com.top5.web.*" })
@Import({ SecurityConfig.class, WebConfig.class })
public class AppConfig {
	
	@Bean(name = "dataSource")
	public DriverManagerDataSource dataSource() {
	    
		//EnvVars enVars = new EnvVars();
		String dbURL = "jdbc:mysql://localhost:3306/top5";/*enVars.getDbUrl("OPENSHIFT_MYSQL_DB_HOST", "OPENSHIFT_MYSQL_DB_PORT", "DB_NAME");*/
		String sqlUser = "root";/*enVars.getEVarByName("SQL_USER");*/
		String sqlPass = "math101"; /*enVars.getEVarByName("SQL_PASS");*/
		
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
	    driverManagerDataSource.setDriverClassName("com.mysql.jdbc.Driver");
	    driverManagerDataSource.setUrl(dbURL);
	    driverManagerDataSource.setUsername(sqlUser);
	    driverManagerDataSource.setPassword(sqlPass);
	    return driverManagerDataSource;
	}
	
	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
}
