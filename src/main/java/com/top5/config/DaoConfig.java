package com.top5.config;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.top5.dao.ItemDAO;
import com.top5.dao.UserDAO;
import com.top5.dao.impl.JdbcItemDAO;
import com.top5.dao.impl.JdbcUserDAO;
import com.top5.util.EnvVars;

public class DaoConfig {
	
	public DriverManagerDataSource dataSource() {
		    
		//EnvVars enVars = new EnvVars();
		/*
		String dbURL = enVars.getDbUrl("OPENSHIFT_MYSQL_DB_HOST", "OPENSHIFT_MYSQL_DB_PORT", "DB_NAME");
		String sqlUser = enVars.getEVarByName("SQL_USER");
		String sqlPass = enVars.getEVarByName("SQL_PASS");
		*/
		String dbURL = "jdbc:mysql://localhost:3306/top5";/*enVars.getDbUrl("OPENSHIFT_MYSQL_DB_HOST", "OPENSHIFT_MYSQL_DB_PORT", "DB_NAME");*/
		String sqlUser = "root";/*enVars.getEVarByName("SQL_USER");*/
		String sqlPass = "math101"; /*enVars.getEVarByName("SQL_PASS");*/
		
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
	    driverManagerDataSource.setDriverClassName("com.mysql.jdbc.Driver");
	    driverManagerDataSource.setUrl(dbURL);
	    driverManagerDataSource.setUsername(sqlUser);
	    driverManagerDataSource.setPassword(sqlPass);
	    return driverManagerDataSource;
	}
	
	
	public UserDAO userDAO(){
		JdbcUserDAO impl = new JdbcUserDAO();
		impl.setDataSource( dataSource() );
		return impl;	 
	}
	
	
	public ItemDAO itemDAO(){
		
		JdbcItemDAO impl = new JdbcItemDAO();
		impl.setDataSource( dataSource() );
		return impl;	 
	}

}