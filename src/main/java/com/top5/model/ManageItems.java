package com.top5.model;

import java.io.IOException;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.servlet.ModelAndView;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.top5.beans.Item;
import com.top5.beans.User;
import com.top5.beans.ResetPriorities;
import com.top5.dao.ItemDAO;
import com.top5.dao.UserDAO;


import com.top5.util.NearestMonday;
import com.top5.config.DaoConfig;

public class ManageItems {
	
	
	public ModelAndView createManageItemsPage(String username){
		
		DaoConfig appConfig = new DaoConfig();
		ItemDAO itemDAO = (ItemDAO) appConfig.itemDAO();
		UserDAO userDAO = (UserDAO) appConfig.userDAO();
		
		Item[] items = itemDAO.getItems(username, -1);
		User user = userDAO.findByUserId(username);
		String disName = user.getDisplayName();
		
		int nextNumber = items.length+1;
		if(items.length==1 && items[0]==null)
			nextNumber=1;
		
		NearestMonday nearestMonday = new NearestMonday();
		String thisWeek = nearestMonday.closestToNow();
		String weekForUser = nearestMonday.closestToTimestamp( user.getUpdateConfirm() );
		Boolean needConfirmUpdate = !thisWeek.equals(weekForUser);
		
		
		ModelAndView model = new ModelAndView();
		model.addObject( "command", new Item() );
		model.addObject("needConfirmUpdate", needConfirmUpdate);
		model.addObject("thisWeek", thisWeek);
		model.addObject("dName", disName);
		model.addObject("allItems", items);
		model.addObject( "nextNumber", nextNumber );
		model.setViewName("manageItems");
		return model;
	}
	
	public void postItem(String json, String username)throws JsonParseException, JsonMappingException, IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		Item item = mapper.readValue( json, Item.class ); 
	    //set the user name for the new item
	  	item.setUsername(username);
		
		DaoConfig appConfig = new DaoConfig();
		ItemDAO itemDAO = (ItemDAO) appConfig.itemDAO();
	  	
		Item[] items = itemDAO.getItems(username, -1);
		
		if(item.getPriority() != items.length+1 && items[0] != null){
			items = localPriorityUpdate(item, items, true);
			itemDAO.updatePriorities(items);
		}
		
		//Finally, insert the item
		itemDAO.insert(item);
	}
	
	public void updateItem(String json) throws JsonParseException, JsonMappingException, IOException{
		
		DaoConfig appConfig = new DaoConfig();
		ItemDAO itemDAO = (ItemDAO) appConfig.itemDAO();
		
		ObjectMapper mapper = new ObjectMapper();
		Item item = mapper.readValue( json, Item.class );
		
		itemDAO.updateItem(item);
	}
	
	public void deleteItem(int item_id){
		
		String username="";
	  	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  	if (!(auth instanceof AnonymousAuthenticationToken)) {
	  		UserDetails userDetail = (UserDetails) auth.getPrincipal();
	  		username = userDetail.getUsername();
	  	}
	  	
	  	Item item = new Item();
		
		DaoConfig appConfig = new DaoConfig();
		ItemDAO itemDAO = (ItemDAO) appConfig.itemDAO();
	  	
		Item[] items = itemDAO.getItems(username, -1);
		
		for(Item i : items){
			if(i.getItem_id() == item_id)
				item = i;
		}
		
		if(item.getItem_id() == 0){
			System.out.println("We couldn't set the item that way, Dave.");
		}
		
		if(item.getPriority() != items.length && items[0] != null){
			items = localPriorityUpdate(item, items, false);
			//adjust the priorities
			itemDAO.updatePriorities(items);
		}
		
		//Finally, insert the item
		itemDAO.delete(item);
		//grab an updated list
		items = itemDAO.getItems(username, -1);
	}
	
	private Item[] localPriorityUpdate(Item newItem, Item[] items, Boolean positiveChange){
		
		int modifier = 1;
		if(!positiveChange)
			modifier = -1;
		
		for(Item item : items){
			
			if( item.getPriority() >= newItem.getPriority() )
				item.setPriority( item.getPriority() + modifier );
		}
		
		return items;
	}
	
	public void reOrderItems(String json) throws IOException {
		
		String username="";
	  	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  	if (!(auth instanceof AnonymousAuthenticationToken)) {
	  		UserDetails userDetail = (UserDetails) auth.getPrincipal();
	  		username = userDetail.getUsername();
	  	}
		
		ObjectMapper mapper = new ObjectMapper();
		ResetPriorities reset = mapper.readValue( json, ResetPriorities.class );
		
		int[] pList = reset.getPrioritizedList();
		//Get all objects
		DaoConfig appConfig = new DaoConfig();
		ItemDAO itemDAO = appConfig.itemDAO();
		Item[] items = itemDAO.getItems(username, -1);
		
		//Loop through all of the user's items
		for(int item=0;item<items.length;item++){
			//Check the id number against the new list
			for(int i=0;i<pList.length;i++){
				//if id_numbers match, then use the pList index
				//to set the new priority value
				if( pList[i]==items[item].getItem_id() ){
					items[item].setPriority(i+1);
					break;
				}
			}
			
		}
		//Now that the items have had their priorities updated
		//Send them back to the database for an update
		itemDAO.updatePriorities( items );
	}
	
	/*
	private String toJson(Person person) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String value = mapper.writeValueAsString(person);
            // return "["+value+"]";
            return value;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
	*/
}