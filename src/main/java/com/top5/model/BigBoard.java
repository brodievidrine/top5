package com.top5.model;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.servlet.ModelAndView;

import com.top5.beans.User;
import com.top5.dao.UserDAO;
import com.top5.beans.Item;
import com.top5.dao.ItemDAO;
import com.top5.beans.BigBoardUser;
import com.top5.config.DaoConfig;

public class BigBoard {
	
	private ModelAndView model;
	
	public BigBoard(){
		
		DaoConfig dConfig = new DaoConfig();
		UserDAO userDAO = dConfig.userDAO();
		ItemDAO itemDAO = dConfig.itemDAO();
		
		String username="";
	  	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  	if (!(auth instanceof AnonymousAuthenticationToken)) {
	  		UserDetails userDetail = (UserDetails) auth.getPrincipal();
	  		username = userDetail.getUsername();
	  	}
		
	  	User user = userDAO.findByUserId(username);
	  	String disName = "n/a";
	  	if(user!=null) 
	  		disName = user.getDisplayName();
	  	
		User[] cList = userDAO.getUserList(false);
		
		//Create the appropriate # of user objects for the main page
		BigBoardUser[] fpUsers = new BigBoardUser[cList.length];
		
		//Run through every user and get their top 5 items
		for(int i=0;i<cList.length;i++){
			if(!cList[i].getEnabled())
				continue;
			String dName = cList[i].getDisplayName();
			BigBoardUser fpU = new BigBoardUser();
			fpU.setDisplayname( dName );
			fpU.setForWeek( cList[i].getForWeek() );
			
			Item[] usersItems = itemDAO.getItems(cList[i].getUserName(), 5);
			String[] titles = new String[usersItems.length];
			String[] descrips = new String[usersItems.length];
			String[] colors = new String[usersItems.length];
			
			//Sort their item's titles and descriptions into arrays
			for(int itemIndex=0; itemIndex<usersItems.length;itemIndex++){
				if(usersItems[itemIndex]==null) break;
				titles[itemIndex] = usersItems[itemIndex].getTitle();
				descrips[itemIndex] = usersItems[itemIndex].getMessage();
				colors[itemIndex] = usersItems[itemIndex].getColor();
			}
			//Then pass those arrays to the fpUsers 
			fpU.setItemTitles(titles);
			fpU.setItemDescriptions(descrips);
			fpU.setItemColors(colors);
			fpUsers[i] = fpU;
		}
		
		model = new ModelAndView();
		model.addObject("title", "Ascend Top 5");
		model.addObject("dName", disName);
		model.addObject("allUsers", fpUsers);
		model.setViewName("bigBoard");
	}
	
	public ModelAndView getModel(){
		
		return model;
	}
}
