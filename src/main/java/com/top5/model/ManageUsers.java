package com.top5.model;

import java.io.IOException;



import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.top5.beans.User;
import com.top5.config.DaoConfig;
import com.top5.dao.ItemDAO;
import com.top5.dao.UserDAO;


public class ManageUsers {
	
	public ModelAndView getManageUsersPage(){
		
		DaoConfig appConfig = new DaoConfig();
		UserDAO userDAO = (UserDAO) appConfig.userDAO();
		
		String username="";
	  	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  	if (!(auth instanceof AnonymousAuthenticationToken)) {
	  		UserDetails userDetail = (UserDetails) auth.getPrincipal();
	  		username = userDetail.getUsername();
	  	}
		
	  	User user = userDAO.findByUserId(username);
	  	String disName = user.getDisplayName();
			
		
		User[] cList = userDAO.getUserList(true);
		
		ModelAndView model = new ModelAndView();
		model.addObject( "command", new User() );
		model.addObject("dName", disName);
		model.addObject("allUsers", cList);
		model.setViewName("manageUsers");
		
		return model;
	}

	public void postUser( String json )throws JsonParseException, JsonMappingException, IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		User user = mapper.readValue( json, User.class );
		
		DaoConfig appConfig = new DaoConfig();
		UserDAO userDAO = (UserDAO) appConfig.userDAO();
	  	
		//Finally, insert the item
		userDAO.insert(user);
	}
	
public void updateUser(String json)throws JsonParseException, JsonMappingException, IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		User user = mapper.readValue( json, User.class );
		
		DaoConfig appConfig = new DaoConfig();
		UserDAO userDAO = (UserDAO) appConfig.userDAO();
	  	
		//Finally, insert the item
		userDAO.update(user);
	}

public void updateConfirm(String username){
	
	
	DaoConfig appConfig = new DaoConfig();
	UserDAO userDAO = (UserDAO) appConfig.userDAO();
  	
	//Finally, insert the item
	userDAO.updateConfirm(username);
}

public void enableUser(String json)throws JsonParseException, JsonMappingException, IOException{
	
	ObjectMapper mapper = new ObjectMapper();
	User user = mapper.readValue( json, User.class );
	
	DaoConfig appConfig = new DaoConfig();
	UserDAO userDAO = (UserDAO) appConfig.userDAO();
  	
	//Finally, insert the item
	userDAO.enable(user);
}

public void authorizeUser(String json)throws JsonParseException, JsonMappingException, IOException{
	
	ObjectMapper mapper = new ObjectMapper();
	User user = mapper.readValue( json, User.class );
	
	DaoConfig appConfig = new DaoConfig();
	UserDAO userDAO = (UserDAO) appConfig.userDAO();
  	
	//Finally, insert the item
	userDAO.authorize(user);
}

public void deleteUser(String json)throws JsonParseException, JsonMappingException, IOException{
	
	ObjectMapper mapper = new ObjectMapper();
	User user = mapper.readValue( json, User.class );
	
	DaoConfig appConfig = new DaoConfig();
	UserDAO userDAO = (UserDAO) appConfig.userDAO();
	ItemDAO itemDAO = (ItemDAO) appConfig.itemDAO();
	
	itemDAO.deleteAllUserItems( user.getUserName() );
	
	//Need to delete all of the users items
	userDAO.delete(user);
}
	
}
