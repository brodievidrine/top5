CREATE DATABASE  IF NOT EXISTS `top5` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `top5`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: top5
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `message` text NOT NULL,
  `posted_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `priority` tinyint(3) unsigned NOT NULL,
  `color` varchar(45) NOT NULL DEFAULT 'white',
  PRIMARY KEY (`item_id`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1110 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'bvidrine','Teacher Dashboard Code','Keep an eye on the teacher dashboard as Chandra wraps up the teacher dashboard.\n\n4/28 - Reviewed model and view with Chandra.','2015-05-01 02:48:28',18,'green'),(2,'bvidrine','QA Canyon Escape','5/26 - Trying to wrap up first round of QA. Doing research on best way to scroll a tile map. Almost done.\n\n6/1 - Added camera object, which allows for more efficient graphics processing. Runs smoothly on IE and iPad now. Wrapping up last QA feedback now.','2015-05-01 03:05:11',16,'yellow'),(6,'bvidrine','Indiana Jane','Spec out and start on Joseph\'s exploration idea.','2015-05-01 03:14:43',21,'white'),(7,'bvidrine','Ascend Marketing App','Establish how we\'ll get this back through the market.','2015-05-01 03:15:30',22,'white'),(124,'bvidrine','Explore 2064 Fix','6/1 - Add a fixed problem set to this exploration. Necessary because we don\'t want reducible problems.','2015-06-01 17:27:33',20,'white'),(125,'bvidrine','Pearson Player Update','Adjusting Pearson player to use new video size/dimensions. ','2015-06-01 17:29:03',19,'white'),(148,'bvidrine','Unit Completion Bug','6/8 - Trying to help Sabine figure out why unit flags and report meters are off.\n\nUpdate: It seems like splicing units into the middle of a study plan could be the culprit. She will test further and create a document so I can focus my debugging.','2015-06-09 10:54:37',17,'white'),(163,'bvidrine','New Base Camp Levels','Create new levels for Base Camp games. To be sold in store.','2015-06-15 15:10:01',10,'white'),(168,'sbucknor','Cecil Quizzes - School Admin','','2015-06-17 14:24:16',1,'white'),(170,'sbucknor','Content Formatting','','2015-06-17 14:25:27',4,'white'),(193,'sbucknor','\"Great Start\" Email Wks 1-12','','2015-06-26 16:33:33',2,'white'),(200,'sbucknor','Scrum projects with team','','2015-06-29 14:54:29',5,'white'),(202,'bvidrine','Happy Students Edit','Create an upbeat 30 second testimonial for the sales meeting.','2015-06-29 17:23:27',15,'white'),(203,'bvidrine','Student Tracker GUI','Create a gui for the live student tracker.','2015-06-29 17:31:28',14,'white'),(215,'bvidrine','iOS 9.2 Beta Test/Fix','Investigate whether or not latest iOS needs a kludge to work with our video.','2015-07-07 14:57:20',12,'white'),(269,'bvidrine','Prepare Other Games','Modify the size and look of Tank Commander, using the template. Add to AscendTest.','2015-08-10 14:20:11',13,'white'),(299,'bvidrine','Store BACK in Base Camp','Get the Base Camp store back into Production.','2015-08-24 14:56:53',7,'white'),(331,'bvidrine','Mini Presentations','Working on 2 presentations for scaffolding & support. ','2015-09-08 13:55:48',8,'white'),(383,'sbucknor','process UUSs as needed','','2015-10-09 22:26:35',3,'white'),(448,'bvidrine','Base Camp Exploit','No cheating! Get outta camp when your time is up.','2015-11-16 16:05:53',11,'white'),(453,'medgren','What Does Success Look Like call','','2015-11-20 15:16:38',4,'white'),(564,'bvidrine','Base Camp Store Spec','Work with Mike, Kevin, and Carl to determine what first version of store will look like and sell.','2016-01-29 23:08:03',9,'white'),(565,'bvidrine','iPad Bug, Interactive Video','HTML elements become useless in interactive question module. creating synthetic elements via the canvas.','2016-01-29 23:09:15',2,'green'),(566,'bvidrine','Award Wall Wrap Up','Finish up with Treasure Room Awards wall UI now that we\'ve decided everything.','2016-01-29 23:11:27',6,'white'),(644,'sabine','Alignment Reports 6.0','','2016-03-07 21:49:40',9,'white'),(671,'jhartman','12T calls','','2016-03-23 13:24:25',5,'white'),(678,'jhartman','Bermuda pilot fu','','2016-03-30 15:57:15',10,'white'),(699,'drichardson','PA e-mail','About Keystone','2016-04-08 20:05:11',2,'white'),(713,'drichardson','NCTM 30 for 30 letter','','2016-04-18 13:44:29',1,'white'),(715,'jhartman','ZOOM (CCSD)','','2016-04-18 13:57:54',13,'white'),(752,'bvidrine','Circumference Explore','Complete circumference to linear distance exploration.\n\n--Need problems','2016-05-02 14:12:17',5,'red'),(761,'drichardson','Follow up with Bright Star','','2016-05-09 12:10:37',4,'white'),(789,'jhartman','Cookville HS TN fu','','2016-05-23 14:07:41',12,'white'),(798,'kbriley','Sales &amp; Marketing Plan','Compare Budget Weights','2016-05-27 23:52:25',4,'red'),(804,'drichardson','Close Deep Creek','','2016-05-31 13:26:30',5,'white'),(843,'ccrawford','Algebra 1 Content Update Plan','','2016-06-20 17:35:48',3,'white'),(846,'medgren','Innov8 script','','2016-06-24 13:53:13',1,'white'),(847,'medgren','Why AM different students update','','2016-06-24 13:53:19',5,'white'),(867,'drichardson','','','2016-07-08 16:08:38',6,'white'),(872,'jhartman','White Pine renewal','','2016-07-11 13:41:22',11,'white'),(878,'evidrine','Question QA','Get accurate count of objs and questions that need to be updated','2016-07-12 14:38:27',6,'white'),(889,'evidrine','Have a baby','','2016-07-25 06:05:42',1,'green'),(915,'bvidrine','Maven Configuration','Setting up Ascend to deploy as a WAR file.','2016-08-01 16:42:53',1,'green'),(924,'sabine','Formatting Updates','','2016-08-08 13:15:04',10,'white'),(941,'kbriley','Complete D-Admins transition','','2016-08-14 04:30:01',5,'green'),(968,'jhartman','Giddings Elem fu','','2016-08-29 13:50:04',6,'white'),(974,'evidrine','Sullivan Captions','Send videos to transcription','2016-08-30 00:10:04',5,'white'),(976,'sabine','D-Admin for customers','Follow up with D-administrators','2016-08-30 00:22:33',1,'white'),(978,'sswaroop','YourKit - Brush Up ','Brush up Yourkit  in low time ','2016-08-30 17:27:21',5,'white'),(981,'jhartman','Cedars LRT','','2016-09-06 13:52:23',4,'white'),(987,'jcachur','check on current 30 for 30','Elgin- would like to sch a reports overview wed thur or fri ','2016-09-06 14:54:56',1,'white'),(996,'sswaroop','Review the DB ','Review the whole DB ','2016-09-09 16:10:20',3,'white'),(1002,'sswaroop','Smoke Automation ','Automate Smoke testing test case.','2016-09-12 10:01:04',4,'white'),(1008,'bvidrine','Chromebook Audio Bug','Debug audio issue occurring on Chromebooks','2016-09-12 15:25:16',3,'yellow'),(1011,'kbriley','2015 DMTC Paid in 2016','','2016-09-16 21:07:26',1,'yellow'),(1014,'sabine','Trademark','NEXT!','2016-09-19 12:47:26',8,'white'),(1015,'sabine','Books and HR (notes)','LADM 2015 PITA, AJE\'s, Invoices, A/R, Credit Card Recons, BXS Recons, Royalties, 401(k) Forms, Humana New plan?, company expense allocation with recurring items,','2016-09-19 12:50:14',3,'white'),(1016,'sabine','Trademark','Finish Ascend, start MLE','2016-09-19 12:57:03',11,'white'),(1017,'medgren','November Newsletter','','2016-09-19 13:26:37',3,'white'),(1018,'medgren','Bibb Live Webinar ','','2016-09-19 13:29:49',2,'white'),(1026,'evidrine','Sullivan Captions Pt2','Review transcription and upload to Felice for translation','2016-09-26 13:43:11',4,'white'),(1027,'mbriley','Interviews','','2016-09-26 13:49:07',1,'yellow'),(1033,'sabine','UUS Plan','','2016-09-28 12:49:28',6,'white'),(1038,'jhartman','CCSD Lynn Trask','','2016-10-03 13:40:27',9,'white'),(1039,'jhartman','CCSD Adult Rose','','2016-10-03 13:40:37',7,'white'),(1047,'rpalms','Mobile Email/Form Best Practices','','2016-10-07 15:44:47',1,'yellow'),(1055,'mbriley','Review CX Procedures','','2016-10-08 22:34:48',2,'white'),(1058,'evidrine','Get some sleep','','2016-10-10 16:06:07',2,'red'),(1059,'evidrine','Do other stuff later','','2016-10-10 16:06:30',3,'green'),(1060,'bvidrine','Preassessment VO ','--Need scripts','2016-10-10 16:19:56',4,'red'),(1062,'sabine','LADM 2018.5','','2016-10-10 16:37:13',4,'white'),(1065,'ccrawford','MG PM 1E Final ESP Captions','','2016-10-14 19:53:37',4,'white'),(1070,'mbriley','Rep Workshop Schedule','','2016-10-17 13:50:43',3,'yellow'),(1074,'jhartman','Del Valle Pilots ','','2016-10-17 14:30:07',2,'white'),(1075,'jhartman','CCSD Jacobson Elem','','2016-10-17 14:30:57',8,'white'),(1080,'sswaroop','End of level MSG','End of level MSG, testing all scenario','2016-10-24 13:07:38',1,'white'),(1084,'jcachur','Sch onsite w/those who','Could not make the events but seemed to want me onsite.  Need to sch these this week. ','2016-10-24 13:36:59',2,'white'),(1085,'jhartman','NCTM Phoenix fu','','2016-10-24 14:12:53',1,'white'),(1088,'kbriley','Algebra I triage & edit','','2016-10-28 21:32:51',2,'green'),(1090,'rpalms','Email Storage','','2016-10-28 22:27:37',2,'white'),(1091,'rpalms','Update CPL','','2016-10-28 22:27:46',3,'white'),(1092,'rpalms','Bibb County Flyer','','2016-10-28 22:27:56',4,'white'),(1093,'rpalms','Innov8 Shipping','','2016-10-28 22:29:20',5,'white'),(1094,'jcachur','Meet with Comm Cons Dist 59','off site meeting at Panera Bread at 11:30 today  CIndy Placko','2016-10-31 12:59:56',3,'red'),(1095,'jcachur','Lakeside Elem - reports overview','1:30 today','2016-10-31 13:00:24',4,'yellow'),(1096,'jcachur','Fanning Elem- Intro Web','Part f St. Louis PS on Wedat 10:45','2016-10-31 13:01:12',5,'green'),(1098,'mbriley','Pulaski Pres','','2016-10-31 14:31:14',4,'green'),(1099,'mbriley','Next Steps Red in Activity','','2016-10-31 15:43:40',5,'yellow'),(1100,'ccrawford','Aurora Alpha Test Plan','','2016-10-31 15:45:31',5,'white'),(1101,'ccrawford','Edit Algebra I Video','','2016-10-31 15:45:43',2,'white'),(1102,'ccrawford','Ascend Server for  Innov8','','2016-11-04 15:50:05',1,'white'),(1103,'kbriley','Hire CX Supervisor','','2016-11-07 05:20:52',3,'white'),(1104,'drichardson','Schedule Alice Cook demo','Montgomery County','2016-11-07 12:36:07',3,'white'),(1105,'jhartman','CCSD opps','','2016-11-07 15:01:14',3,'white'),(1106,'sswaroop','Siadmin Left Nav bar ','Siadmin Left Nav bar Testing.','2016-11-07 16:09:48',2,'white'),(1107,'sabine','Algebra 1 Content','','2016-11-09 13:40:41',7,'white'),(1108,'sabine','2 Specs and a Plan ','One Level Objectives, reporting for deactivated students, UUS Plan','2016-11-09 13:40:53',5,'white'),(1109,'sabine','Audio for Pre Assessment','','2016-11-09 13:46:43',2,'white');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`user_role_id`),
  UNIQUE KEY `uni_username_role` (`role`,`username`),
  KEY `fk_username_idx` (`username`),
  CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'bvidrine','ROLE_ADMIN'),(2,'ccrawford','ROLE_ADMIN'),(3,'kbriley','ROLE_ADMIN'),(16,'drichardson','ROLE_USER'),(18,'evidrine','ROLE_USER'),(17,'jcachur','ROLE_USER'),(15,'jhartman','ROLE_USER'),(12,'jvilla','ROLE_USER'),(7,'mbriley','ROLE_USER'),(8,'medgren','ROLE_USER'),(9,'rpalms','ROLE_USER'),(4,'sabine','ROLE_USER'),(13,'sbucknor','ROLE_USER'),(19,'sswaroop','ROLE_USER');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `displayname` varchar(45) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `updateconfirm` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('bvidrine','money','Brodie Vidrine',1,'2016-11-07 15:58:15'),('ccrawford','Math1010','Carl Crawford',1,'2016-11-04 15:47:55'),('drichardson','math1398','Dan Richardson',1,'2016-11-07 12:37:14'),('evidrine','math101','Beth Vidrine',1,'2016-10-10 16:06:41'),('jcachur','math1777','Joanna Cachur',1,'2016-10-31 13:01:18'),('jhartman','math1492','Jeff Hartman',1,'2016-11-07 15:02:51'),('jvilla','math1206','Jenna Villa',0,'2015-07-24 17:05:17'),('kbriley','bossman1','Kevin Briley',1,'2016-11-07 05:21:04'),('mbriley','math1438','Marjorie Briley',1,'2016-10-31 14:31:27'),('medgren','math1509','Mike Edgren',1,'2016-11-04 15:51:37'),('rpalms','math1643','Renata Palms',1,'2016-10-28 22:29:22'),('sabine','Math101','Sabine Koerner',1,'2016-11-09 13:40:18'),('sbucknor','math3131','Shanfer Bucknor',1,'2016-11-04 22:09:55'),('sswaroop','math1777','Sai Swaroop',1,'2016-11-07 16:11:31');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'top5'
--

--
-- Dumping routines for database 'top5'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-09 16:23:56
